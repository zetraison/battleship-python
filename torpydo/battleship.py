import random

import colorama
from colorama import Fore, Back, Style

from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController

myFleet = []
enemyFleet = []
yourTurns = []
enemyTurns = []
enemyFleetPositions = []
myFleetPositions = []

def main():
    colorama.init()
    print(Fore.YELLOW + r"""
───────────────▌
─────────▄▄▄▄▄██▄▄▄▄▄───────▌
─────────░░░░░░░░░░░░─────▄▄█▄▄
─────────░░░░░░░░░░░░─────░░░░░
─────────░░░░░░░░░░░░────▄▄▄█▄▄▄
─────────░░░░░░░░░░░░────░░░░░░░
───────▄▄▄▄▄▄▄██▄▄▄▄▄▄▄──░░░░░░░
───────░░░░▄░░░░░░▄░░░░─▄▄▄▄█▄▄▄▄
───────░░░░░▄▀▀▀▀▄░░░░░─░░░░░░░░░
───────░░░░░▌▀░░▀▐░░░░░─░░░░░░░░░
───────░░░░▄░▀▄▄▀░▄░░░░─░░░░░░░░░
───────░░░░░░░░░░░░░░░░─░░░░░░░░░────▄███▀
▀████▄▄───────██────────────█─────▄███▀
───▀▀██████▄██████▄─▄▄─▄▄─▄███▄▄████▀
──────▀███▀█▀█▀█▀█▀████████████████▀
───────▀██████████████████████████▀
╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦
╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦
╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦╩╦
╦╩╦╩╦╩╦╩ Welcome to Battleship ╦╩╦╩╦╩╦╩╦╩""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def start_game():
    global myFleet, enemyFleet, enemyFleetPositions, myFleetPositions, yourTurns

    print(Fore.WHITE, r'''
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄▄░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▄███░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░▄▄▄███████░░░
░░░░░░░░░░░░░░░░░░░░░▄▄██████████████▄░
░░▄░░░░░░░░░░░░░▄▄████████████████████▄
░██░░░░░░░▄▄▄█████████████████████▀▀▀▀▀
▄█░░░░▄░▀███████████████████████▀░░░░░░
█▀░░▄███░▀███████████████████▀▀░░░░░░░░
█░░▄█████░▀█████▀▀▀████████▀░░░░░░░░░░░
█░░███████▄░▀▀▄▄▄▄▄▄▄░▀█▀▀░░░░░░░░░░░░░
▀█▄████████░▄█████████▄░░░░░░░░░░░░░░░░
░░░▀██████░▄████▀▀▀▀████░░░░░░░░░░░░░░░
░░░░▀█████░████░░░░░████░░░░░░░░░░░░░░░
░░░░░░▀▀██░████▄░░░▄████░░░░░░░░░░░░░░░
░░░░░░░░░░░▀███████████▀░░░░░░░░░░░░░░░
░░░░░░░░░░░░░▀▀█████▀▀░░░░░░░░░░░░░░░░░
░░░░░░░░ Ready for the volley! ░░░░░░░░''')

    for fleet in enemyFleet:
        enemyFleetPositions += fleet.positions

    for fleet in myFleet:
        myFleetPositions += fleet.positions

    while True:
        print()

        xAxisLabels = []
        yAxisLabels = [" "]
        for let in Letter:
            yAxisLabels.append(let.name)
            xAxisLabels.append(let.value)
        print(Fore.WHITE, " ".join(yAxisLabels))
        for i in Letter:
            xAxis = [str(i.value)]
            for j in Letter:
                xAxis.append("*")
            print(Fore.WHITE, " ".join(xAxis))

        if len(yourTurns) > 0:
            print(Fore.WHITE, "\nYour previous turns: [%s]\n" % ",".join([str(turn) for turn in yourTurns]))

        print(Fore.WHITE, "\nPlayer, it's your turn\n")

        try:
            yourPosition = parse_position(input("\n>>> Enter coordinates for your shot :".upper()))
        except KeyError:
            print(Fore.WHITE, "\nPosition is out of range. Try again")
            continue
        except ValueError:
            print(Fore.RED, "\nIncorrect input. Try again")
            continue
        if yourPosition.row > len(Letter) or yourPosition.row < 0:
            print(Fore.WHITE, "Position is out of range. Try again")
            continue

        yourTurns.append(yourPosition)

        is_hit = GameController.check_is_hit(enemyFleet, yourPosition)
        if is_hit:
            print(Fore.RED, r'''
██████████▀▀▀▀▀▀▀▀▀▀▀███████████
██████▀▀▀░░░░░░░░░░░░░▀▀████████
███▀░░░░░░░▄▄▄░░░▄▄▄░░░░░░░▀████
███░░░░░▄▄███████████▄▄▄░░░░░███
███░░░░████▀▀▀▀▀▀▀▀▀█████▄░░░███
████▄▄░█▀▀░░▄▄░░▄▄░░░░░██▀░░▄███
████████░░▄▄█████████░░█████████
████████▄▄██▀░░░░░▀█████████████
█████████████▄░░░▄██████████████
██████████████░░░███████████████
██████████████░░░███████████████
█████████████ Hit ██████████████''')

        print(Fore.WHITE, "Yeah ! Nice hit !" if is_hit else "Miss")
        if not is_hit:
            print(Fore.BLUE + r"""
_____________________oo$$$$$$$$$$$$$$$$$$$$$$$$o__________________________ 
___________________oo$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o_________o$___$$_o$___ 
___o_$_oo________o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o_______$$_$$_$$o$__ 
oo_$_$__$______o$$$$$$$$$____$$$$$$$$$$$$$___$$$$$$$$$o_______$$$o$$o$____ 
_$$$$$$o$_____o$$$$$$$$$______$$$$$$$$$$$______$$$$$$$$$$o____$$$$$$$$____ 
__$$$$$$$____$$$$$$$$$$$______$$$$$$$$$$$______$$$$$$$$$$$$$$$$$$$$$$$____ 
__$$$$$$$$$$$$$$$$$$$$$$$____$$$$$$$$$$$$$____$$$$$$$$$$$$$$_____$$$______
____$$$___$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$_____$$$_______ 
____$$$___o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$______$$$o___ 
___o$$____$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$_______$$$o__ 
___$$$____$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$___$$$$$$ooooo$$$$o 
__o$$$oooo$$$$$__$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$___o$$$$$$$$$$$$$$$$$ 
__$$$$$$$$"$$$$___$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$_____$$$$_____________ 
__________$$$$____"$$$$$$$$$$$$$$$$$$$$$$$$$$$$______o$$$_________________
____________$$$o________$$$$$$$$$$$$$$$$$$$$__________$$$_________________
______________$$$o__________$$__$$$$$$_________________o$$$_______________
_______________$$$$o________________________________o$$$__________________
________________"$$$$o______o$$$$$$o_$$$$o________o$$$$___________________
__________________"$$$$$oo_______$$$$o$$$$$o___o$$$$______________________
_______________________$$$$$oooo___$$$o$$$$$$$$$__________________________
__________________________$$$$$$$oo_$$$$$$$$$$____________________________
____________________________________$$$$$$$$$$$___________________________ 
____________________________________$$$$$$$$$$$$__________________________ 
_____________________________________$$$$$$$$$$"__________________________ 
_______________________________________$$$$$$$____________________________ 
__________________________________________________________________________
____________________________________ Miss ________________________________""" + Style.RESET_ALL)

        enemyPosition = get_random_position()
        enemyTurns.append(enemyPosition)
        is_hit = GameController.check_is_hit(myFleet, enemyPosition)
        print()
        print(Fore.WHITE, f"Computer shoot in {enemyPosition.column.name}{enemyPosition.row} and {'hit your ship!' if is_hit else 'miss'}")
        if is_hit:
            print(Fore.RED, r'''
██████████▀▀▀▀▀▀▀▀▀▀▀███████████
██████▀▀▀░░░░░░░░░░░░░▀▀████████
███▀░░░░░░░▄▄▄░░░▄▄▄░░░░░░░▀████
███░░░░░▄▄███████████▄▄▄░░░░░███
███░░░░████▀▀▀▀▀▀▀▀▀█████▄░░░███
████▄▄░█▀▀░░▄▄░░▄▄░░░░░██▀░░▄███
████████░░▄▄█████████░░█████████
████████▄▄██▀░░░░░▀█████████████
█████████████▄░░░▄██████████████
██████████████░░░███████████████
██████████████░░░███████████████
█████████████ Hit ██████████████''')

        yourScore, enemyScore = check_game_result(yourPosition, enemyPosition)
        if yourScore:
            print(Fore.WHITE, "You win!")
            break

        if enemyScore:
            print(Fore.WHITE, "Game over!")
            break

def check_game_result(yourPosition, enemyPosition):
    if yourPosition in enemyFleetPositions:
        index = enemyFleetPositions.index(yourPosition)
        del enemyFleetPositions[index]

    if enemyPosition in myFleetPositions:
        index = myFleetPositions.index(enemyPosition)
        del myFleetPositions[index]

    return len(enemyFleetPositions) == 0, len(myFleetPositions) == 0

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    #set_position_to_myFleet(myFleet)

    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print()
        print(f"Please enter the positions for the {ship.name} (size: {ship.size})")

        i = 0
        while i < ship.size:
            try:
                position_input = input(f"Enter position {i} of {ship.size} (i.e A3): ")
                success = ship.add_position(position_input)
                if not success:
                    print(Fore.RED, "\nPosition is out of range. Try again\n" + Style.RESET_ALL)
                    continue
            except ValueError:
                print(Fore.RED, "\nIncorrect input. Try again\n" + Style.RESET_ALL)
                continue
            except KeyError:
                print(Fore.RED, "\nIncorrect input. Try again\n" + Style.RESET_ALL)
                continue
            i += 1


def initialize_enemyFleet():
    global enemyFleet

    enemyFleet = GameController.initialize_ships()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    enemyFleet[0].positions.append(Position(Letter.B, 6))
    enemyFleet[0].positions.append(Position(Letter.B, 7))
    enemyFleet[0].positions.append(Position(Letter.B, 8))

    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))
    enemyFleet[1].positions.append(Position(Letter.E, 9))

    enemyFleet[2].positions.append(Position(Letter.A, 3))
    enemyFleet[2].positions.append(Position(Letter.B, 3))
    enemyFleet[2].positions.append(Position(Letter.C, 3))

    enemyFleet[3].positions.append(Position(Letter.F, 8))
    enemyFleet[3].positions.append(Position(Letter.G, 8))
    enemyFleet[3].positions.append(Position(Letter.H, 8))

    enemyFleet[4].positions.append(Position(Letter.C, 5))
    enemyFleet[4].positions.append(Position(Letter.C, 6))

def set_position_to_myFleet(fleet):
    fleet[0].positions.append(Position(Letter.A, 1))
    fleet[0].positions.append(Position(Letter.B, 4))
    fleet[0].positions.append(Position(Letter.B, 5))
    fleet[0].positions.append(Position(Letter.B, 6))
    fleet[0].positions.append(Position(Letter.B, 7))
    fleet[0].positions.append(Position(Letter.B, 8))

    fleet[1].positions.append(Position(Letter.E, 6))
    fleet[1].positions.append(Position(Letter.E, 7))
    fleet[1].positions.append(Position(Letter.E, 8))
    fleet[1].positions.append(Position(Letter.E, 9))

    fleet[2].positions.append(Position(Letter.A, 3))
    fleet[2].positions.append(Position(Letter.B, 3))
    fleet[2].positions.append(Position(Letter.C, 3))

    fleet[3].positions.append(Position(Letter.F, 8))
    fleet[3].positions.append(Position(Letter.G, 8))
    fleet[3].positions.append(Position(Letter.H, 8))

    fleet[4].positions.append(Position(Letter.C, 5))
    fleet[4].positions.append(Position(Letter.C, 6))

if __name__ == '__main__':
    main()
