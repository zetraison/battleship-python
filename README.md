# Torpydo

A simple game of Battleship, written in Python.

General rules:
Beginning of the game
Two players take part in the game. They take one game set, are placed so as not to see the opponents playing field, and prepare for the game.

Game sets are located as shown in the figure (see the full version of the rules), and ships are located on the game fields.

Attention! Ships should not be in contact, that is, the distance between them should be at least one cell.

Attention! Rearranging your ships after the start of the game is strictly prohibited.

Game progress
After the ships are placed, you can start the game.

There are two main options for the game:

Players shoot strictly in turn.
Players fire before the first miss, that is, if a player hits the enemys ship, he fires the next shot, and only after his miss the move passes to another player.
Place their ships on the playing field and reflect the results of enemy shots. The screen serves to reflect the results of their own shots at the enemy. On the field and on the screen, a miss is indicated by a white chip, and a hit by a red chip. Each shot has its own coordinates. The shooter must name the coordinates loudly and clearly. For example: A4 shot!

The player whose ships are being fired marks a shot on his playing field:

in case of a slip - white
in case of hit - red
Then he clearly announces the result of the shot:

option 1. "Miss!" - the player did not hit the ship
option 2. "Hit!" - the player hit the ship, but did not destroy.
option 3. "Drowned!" - the ship is destroyed.
Having learned the result of his shot, the player marks him with a white or red chip on the screen. To destroy a ship, a certain number of hits is required. It corresponds to the number of holes for red chips on its deck.

The winner is the one who first destroys all the ships of the enemy.

Additional description of the gameplay.

The game is accompanied by visual effects.

When the game starts, a warship is displayed.

When firing, the player displays a ships gun from which he will launch a salvo.

When hit, the player will see an explosion!

If you miss, a small wave from getting into the water will be displayed.

Each move is separate from the other.

At the end of the game, the player displays a message that the game is completed and the result of the game.

If you have questions, please contact us:
tel .: 8-800-890-23-12
mail:battleship.support@gmail.com


# Getting started

This project requires Python 3.6. To prepare to work with it, pick one of these
options:

## Linux and macOS

Create and activate a [virtual environment][venv]. This assumes python3 is
already installed.

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
export PYTHONPATH=.
```

If you stop working on this project, close your shell, and return later, make
sure you run the `source bin/venv/activate` command again.

[venv]:https://docs.python.org/3/library/venv.html

## Windows

[Download][pywin] and install Python 3.6 for Windows. Make sure python is on
your `PATH`. Open a command prompt:

```commandline
python -m venv venv
venv\Scripts\activate.bat
pip install -r requirements.txt
set PYTHONPATH=.
```

[pywin]:https://www.python.org/downloads/windows/

## Docker

If you don't want to install anything Python-related on your system, you can
run the game inside Docker instead.

### Build the Docker Image

```bash
docker build -t torpydo .
```

### Run a Docker Container from the Image

```bash
docker run -it --env PYTHONPATH=/torpydo -v ${PWD}:/torpydo torpydo bash
```

# Launching the game

```bash
python -m torpydo

# alternatively:
python torpydo/battleship.py
```

# Running the Tests

```
nosetests --exe
behave
```

to run with coverage:
```
nosetests --exe --with-coverage
```

to run and store the test results for further examination (e.g. build pipeline)
```
nosetests --exe --with-xunit
behave --junit
```

Run behave tests with coverage:
```
https://stackoverflow.com/a/37447392/736079
```